# ics-ans-java

Ansible playbook to install java 12. This is for test only!
The new java role will be added to the required playbooks when ready.

## Requirements

- ansible >= 2.7
- molecule >= 2.19

## License

BSD 2-clause
